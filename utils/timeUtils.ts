export function validateTime(time: string): boolean {
    const parts = time.split(":");
    if (parts.length !== 2) {
        return false;
    }

    const hoursNumber = Number(parts[0]);
    if (hoursNumber < 0 || hoursNumber > 23) {
        return false;
    }

    const minutesNumber = Number(parts[1]);
    if (minutesNumber < 0 || minutesNumber > 59) {
        return false;
    }

    return true;
}

export function padTime(time: string): string {
    const parts = time.split(":");
    return `${parts[0].padStart(2, "0")}:${parts[1].padStart(2, "0")}`;
}
