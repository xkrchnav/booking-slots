import { dayToDisplay, displayToDay, validateDisplayDay } from "../dayUtils";

test("Day 0315 is displayed correctly 15.3.", () => {
    const day = "0315";
    const result = dayToDisplay(day);
    expect(result).toBe("15.3.");
});

test("Pads 0 day and month correctly", () => {
    const display = "1.2.";
    const result = displayToDay(display);
    expect(result).toBe("0201");
});

test("Validates number of . separators", () => {
    const display ="1.2.";
    const result = validateDisplayDay(display);
    expect(result).toBe(true);
})

test.each(["1.12.", "24.12."])("Valid display days", (display) => {
    const result = validateDisplayDay(display);
    expect(result).toBe(true);
});

test.each(["1.2.3", "134.22", "24.13.", "1.1"])("Invalid display dayes", (display) => {
    const result = validateDisplayDay(display);
    expect(result).toBe(false);
})
