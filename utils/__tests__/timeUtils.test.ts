import { padTime, validateTime } from "../timeUtils";

test.each(["11:30", "2:40", "23:59"])("Should be valid times", (time) => {
    const result = validateTime(time);
    expect(result).toBeTruthy();
});

test.each(["131:-10", "24:40", "23:69"])("Should be invalid times", (time) => {
    const result = validateTime(time);
    expect(result).toBeFalsy();
});

test("Pads time correctly", () => {
    const time = "8:30";
    const result = padTime(time);
    expect(result).toBe("08:30");
});
