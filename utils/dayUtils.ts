/**
 * Transforms day from sortable format to display format.
 */
export function dayToDisplay(day: string): string {
    const monthNumber = Number(day.substring(0, 2));
    const dayNumber = Number(day.substring(2, 4));

    return `${dayNumber}.${monthNumber}.`;
}

/**
 * Transforms day from display format 24.12. to sortable format 1224
 */
export function displayToDay(display: string): string {
    const parts = display.split(".");
    const dayPart = parts[0].padStart(2, "0");
    const monthPart = parts[1].padStart(2, "0");
    return `${monthPart}${dayPart}`;
}

/** Checks whether display date is in valid format dd.MM. */
export function validateDisplayDay(display:string): boolean {
    const parts = display.split(".");
    if(parts.length !== 3 || parts[2] !== "") {
        return false;
    }

    const dayNumber = Number(parts[0]);

    if(dayNumber < 1 || dayNumber > 31) {
        return false;
    }

    const monthNumber = Number(parts[1]);

    if(monthNumber < 1 || monthNumber > 12) { 
        return false;
    }

    return true;
}