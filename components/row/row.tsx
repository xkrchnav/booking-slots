import Link from "next/link";
import { useState } from "react";
import { dayToDisplay } from "../../utils/dayUtils";
import { validateTime } from "../../utils/timeUtils";

export type RowProps = {
    day: string;
};

// TODO: load from db
function getData(): string[] {
    const data = ["11:00", "11:30", "14:30"];
    return data;
}

const Row = ({ day }: RowProps) => {
    const display = dayToDisplay(day);

    const [times, setTimes] = useState<string[]>(() => getData());
    const [newTime, setNewTime] = useState("");
    const [isErrorVisible, setIsErrorVisible] = useState(false);

    function handleAdd() {
        if (!newTime || !validateTime(newTime)) {
            setIsErrorVisible(true);
            return;
        }

        setIsErrorVisible(false);
        const newTimes = [...times, newTime].sort();
        setTimes(newTimes);
        setNewTime("");
    }

    function handleRemove(time: string) {
        setTimes((x) => x.filter((f) => f !== time));
    }

    function handleSave() {
        // TODO: save to db
    }

    return (
        <>
            <div className="container-sm">
                <h1>Změnit termíny pro: {display}</h1>
                {times.map((x, i) => (
                    <div key={i} className="row">
                        <div className="col-6">{x}</div>
                        <div className="col-6">
                            <button
                                type="button"
                                className="btn btn-danger"
                                onClick={() => handleRemove(x)}
                            >
                                Odstranit
                            </button>
                        </div>
                    </div>
                ))}
                <div className="row">
                    <div className="col-6">
                        <input
                            type="text"
                            onChange={(e) => setNewTime(e.target.value)}
                            value={newTime}
                        />
                        {isErrorVisible && (
                            <div style={{ color: "red" }}>
                                Zadejte správny formát času hh:mm.
                            </div>
                        )}
                    </div>
                    <div className="col-6">
                        <button
                            type="button"
                            className="btn btn-success"
                            onClick={handleAdd}
                        >
                            Přidat
                        </button>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <Link href={"/"}>
                            <button type="button" className="btn btn-secondary">
                                Zrušit
                            </button>
                        </Link>

                        <button type="button" className="btn btn-primary">
                            Uložit
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Row;
