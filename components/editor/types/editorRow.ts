export type EditorRow = {
    sortableDay: string;
    sortableTimes: string[];
};
