import Link from "next/link";
import { useState } from "react";
import {
    dayToDisplay,
    displayToDay,
    validateDisplayDay,
} from "../../utils/dayUtils";
import { EditorRow } from "./types/editorRow";

// TODO: load from DB
function loadData(): EditorRow[] {
    const data: EditorRow[] = [
        {
            sortableDay: "0318",
            sortableTimes: ["10:30", "11:00"],
        },
        {
            sortableDay: "0319",
            sortableTimes: ["10:30", "12:00"],
        },
    ];

    return data;
}

const Editor = () => {
    const [rows, setRows] = useState(() => loadData());

    const [deletingIndex, setDeletingIndex] = useState<number>();
    const [newDay, setNewDay] = useState("");
    const [isErrorVisible, setIsErrorVisible] = useState(false);

    function handleRemove(sortableDay: string) {
        setDeletingIndex(rows.findIndex((x) => x.sortableDay === sortableDay));
    }

    function handleCancel() {
        setDeletingIndex(undefined);
    }

    function handleConfirmRemove() {
        // TODO: save to db
        setRows((x) => x.filter((_, i) => i !== deletingIndex));
    }

    function handleAdd() {
        if (!newDay || !validateDisplayDay(newDay)) {
            setIsErrorVisible(true);
            return;
        }

        const sortableDay = displayToDay(newDay);

        const newRows: EditorRow[] = [
            ...rows,
            { sortableDay, sortableTimes: [] },
        ].sort((a, b) => a.sortableDay.localeCompare(b.sortableDay));
        setRows(newRows);
        setNewDay("");
    }

    return (
        <div>
            {rows.map((x, i) => {
                if (deletingIndex != null && deletingIndex === i) {
                    return (
                        <div className="row" key={i}>
                            <div className="col">
                                Určite odstranit{" "}
                                {dayToDisplay(rows[i].sortableDay)}?
                            </div>
                            <div className="col">
                                <button
                                    type="button"
                                    className="btn btn-danger"
                                    onClick={handleConfirmRemove}
                                >
                                    Ano odstranit
                                </button>
                                <button
                                    type="button"
                                    className="btn btn-secondary"
                                    onClick={handleCancel}
                                >
                                    Ne
                                </button>
                            </div>
                        </div>
                    );
                }

                return (
                    <div className="row" key={i}>
                        <div className="col-1">{dayToDisplay(x.sortableDay)}</div>
                        <div className="col-5">{x.sortableTimes.join(" ")}</div>
                        <div className="col-6">
                            <Link href={`/day/${x.sortableDay}`}>
                                <button
                                    type="button"
                                    className="btn btn-primary"
                                >
                                    Změnit
                                </button>
                            </Link>
                            <button
                                type="button"
                                className="btn btn-danger"
                                disabled={deletingIndex != null}
                                onClick={() => handleRemove(x.sortableDay)}
                            >
                                Odstranit
                            </button>
                        </div>
                    </div>
                );
            })}
            <div className="row">
                <div className="col-6">
                    <input
                        type="text"
                        value={newDay}
                        onChange={(e) => setNewDay(e.target.value)}
                    />
                </div>
                <div className="col-6">
                    <button
                        type="button"
                        className="btn btn-success"
                        onClick={handleAdd}
                    >
                        Přidat
                    </button>
                    {isErrorVisible && (
                        <div style={{ color: "red" }}>
                            Zadejte správny formát data dd.MM.
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
};

export default Editor;
