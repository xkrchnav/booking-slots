import type { NextPage } from "next";
import Head from "next/head";
import Editor from "../components/editor/editor";

const Home: NextPage = () => {
    return (
        <div>
            <Head>
                <title>Rezervačný systém</title>
                <meta
                    name="description"
                    content="Rezervačný systém - osvoboď se"
                />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main
                className="container-sm"
            >
                <h1>Editor termínů</h1>
                <Editor />
            </main>
        </div>
    );
};

export default Home;
