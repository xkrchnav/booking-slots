import { useRouter } from "next/router";
import Row from "../../components/row/row";

const Day = () => {
    const router = useRouter();
    const { day } = router.query;

    if (day == null) {
        return <div>Nahrávám</div>;
    }

    return <Row day={day as string} />;
};

export default Day;
